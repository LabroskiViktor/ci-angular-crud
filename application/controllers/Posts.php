<?php

class Posts extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('PostsModel');
	}

	//Load inital view
	public function index() {
		$this->load->view('posts');
	}

	//Get all posts
	public function getPostsData() {
		$posts = new PostsModel;
		$data = $posts->getPosts();
		echo json_encode($data);
	}

	//Insert/Update post
	public function insertUpdatePostsData() {
		$posts = new PostsModel;
		echo $posts->insertUpdatePost();
	}

	//Get single post
	public function getSinglePost($id) {
		$posts = new PostsModel;
		$data = $posts->getSinglePostRow($id);
		echo json_encode($data);
	}

	//Delete current post
	public function deletePostsData($id) {
		$posts = new PostsModel;
		echo $posts->deletePost($id);
	}
}