<?php

class PostsModel extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	//Get all posts
	public function getPosts() {
		$query = $this->db->get('posts');
		return $query->result();
	}

	//Insert/Update post data
	public function insertUpdatePost() {
		$form_data = json_decode(file_get_contents('php://input'));
		$gmt = $form_data->post_timezone;
		$timestamp = strtotime($form_data->datetimepicker . ' ' . ' GMT' . $gmt);
		$converted_date = date('Y-m-d H:i', $timestamp);
		$data = array(
			'post_title' => $form_data->post_title,
			'post_body' => $form_data->post_body,
		);
		if (isset($form_data->post_id)) {
			$this->db->where('id', $form_data->post_id);
			$this->db->update('posts', $data);
			return ($this->db->affected_rows() >= 0) ? 'The Post Was Successfully Updated!' : 'Please Try Again...';
		} else {
			$data['created_date'] = $converted_date;
			$this->db->insert('posts', $data);
			return ($this->db->affected_rows() >= 0) ? 'The Post Was Successfully Created!' : 'Please Try Again...';
		}
	}

	//Get single post
	public function getSinglePostRow($id) {
		$this->db->select('*');
		$this->db->from('posts');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	//Delete post data
	public function deletePost($id) {
		$this->db->where('id', $id);
		$this->db->delete('posts');
		return ($this->db->affected_rows() >= 0) ? 'The Post Was Successfully Deleted!' : 'Please Try Again...';
	}
}