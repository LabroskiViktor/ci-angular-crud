(function() {
	$('#datetimepicker').datetimepicker({
		format: 'Y-m-d H:i',
		formatTime: 'A g:i'
	});
})();
var app = angular.module('posts', []);
app.controller('postsController', function($scope, $http, postsFactory) {
	$scope.btnName = 'Add Post';
	console.log($('#datetimepicker').val());
	//Get All Posts
	postsFactory.getPosts()
		.then(function(response) {
			$scope.get_data = response.data;
		});

	//Insert/Update Posts
	$scope.postData = {};
	$scope.insertUpdatePost = function() {
		if ($scope.insertForm.$valid) {
			postsFactory.insertUpdatePost($scope.postData)
				.then(function(response) {
					if (response.data !== 'Please Try Again') {
						$scope.messageSuccess = response.data;
						$scope.postData = {
							post_title: '',
							post_body: ''
						};
						$scope.btnName = 'Add Post';
						$http.get('Posts/getPostsData')
							.then(function(response) {
								$scope.get_data = response.data;
							});
					} else {
						$scope.messageError = response.data;
					}
				}, function(error) {
					console.log(error);
				});
		}
	}

	//Edit Post
	$scope.editPost = function(event) {
		var rowId = event.currentTarget.getAttribute("edit-id");
		postsFactory.editPost(rowId)
			.then(function(response) {
				$scope.btnName = "Update Post";
				$scope.postData.post_id = response.data[0].id;
				$scope.postData.post_title = response.data[0].post_title;
				$scope.postData.post_body = response.data[0].post_body;
			});
	}

	//Delete Post
	$scope.deletePost = function(event) {
		var rowId = event.currentTarget.getAttribute("delete-id");
		postsFactory.deletePost(rowId)
			.then(function(response) {
				if (response.data !== 'Please Try Again') {
					$scope.messageSuccess = response.data;
					$http.get('Posts/getPostsData')
						.then(function(response) {
							$scope.get_data = response.data;
						});
				} else {
					$scope.messageError = response.data;
				}
			}, function(error) {
				console.log(error);
			});
	}
});