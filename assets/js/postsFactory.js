var app = angular.module('posts');
app.factory('postsFactory', function($http) {

	var dataFactory = {};

	//Get All Posts
	dataFactory.getPosts = function() {
		return $http.get('Posts/getPostsData');
	}

	//Insert/Update Posts
	dataFactory.insertUpdatePost = function(data) {
		return $http({
			method: 'POST',
			url: 'Posts/insertUpdatePostsData',
			data: data,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		});
	}

	//Edit Post
	dataFactory.editPost = function(id) {
		return $http.get('Posts/getSinglePost/' + id)
	}

	//Delete Posts
	dataFactory.deletePost = function(id) {
		return $http({
			method: 'POST',
			url: 'Posts/deletePostsData/' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		});
	}

	return dataFactory;

});